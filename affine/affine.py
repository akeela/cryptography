#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import (QMainWindow, QApplication, QWidget, QPushButton, 
	QTextEdit, QAction, QGridLayout, QLabel, QRadioButton, QLineEdit, QMessageBox,
	)
from PyQt5.QtCore import QCoreApplication
from PyQt5.QtGui import *

class GuiView(QWidget):
	def __init__ (self):
		super().__init__()
		self.textlen = 0
		self.mode = 0

		self.initUI()
		
	#Drawing interface
	def initUI(self):
		self.btn_encrypt = QPushButton ('Зашифровать', self)
		self.btn_decipher = QPushButton ('Расшифровать', self)

		self.btn_help = QPushButton ('Помощь', self)
		self.btn_help.clicked.connect(lambda:self.helpshow())
		lb_opentext = QLabel('Исходный текст')
		self.te_opentext = QTextEdit()

		lb_ciphertext = QLabel('Измененный текст')
		self.te_ciphertext = QTextEdit()

		self.lb_alphabettext = QLabel('Длинна алфавита ')
		self.te_alphabettext  = QTextEdit()
		#Setting text by default
		self.te_alphabettext.setText("ABCDEFGHIJKLMNOPQRSTUVWXYZ")

		lb_modetext = QLabel('Шифр')
		self.rb_affine  = QRadioButton("Аффинный шифр")
		self.rb_affine.setChecked(True)
		self.rb_affine.toggled.connect(lambda:self.btnstate(self.rb_affine))
		self.rb_affinereq  = QRadioButton("Аффинный рекуррентный шифр")
		self.rb_affinereq.toggled.connect(lambda:self.btnstate(self.rb_affinereq))
		lb_ais = QLabel('a1 =')
		lb_a2is = QLabel('a2 =')
		lb_bis = QLabel('b1 =')
		lb_b2is = QLabel('b2 =')
		self.le_ais = QLineEdit() # a
		self.le_a2is = QLineEdit() # a
		self.le_ais.setText("0")
		self.le_a2is.setText("0")
		self.le_bis = QLineEdit() # b
		self.le_b2is = QLineEdit() # b
		self.le_bis.setText("0")
		self.le_b2is.setText("0")

		self.le_a2is.setDisabled(True)
		self.le_b2is.setDisabled(True)

		grid = QGridLayout()
		grid.setSpacing(10)

		grid.addWidget(lb_opentext,1,0)
		grid.addWidget(self.te_opentext,2,0)
		grid.addWidget(lb_ciphertext,1,2)
		grid.addWidget(self.te_ciphertext,2,2,1,2)
		grid.addWidget(self.lb_alphabettext,3,0)
		grid.addWidget(self.te_alphabettext,4,0,6,1)

		#Cipher
		grid.addWidget(lb_modetext,3,2)
		grid.addWidget(self.rb_affine,4,2)
		grid.addWidget(self.rb_affinereq,5,2)
		grid.addWidget(lb_ais,6,1)
		grid.addWidget(lb_bis,7,1)
		grid.addWidget(self.le_ais,6,2)
		grid.addWidget(self.le_bis,7,2)
		grid.addWidget(lb_a2is,8,1)
		grid.addWidget(lb_b2is,9,1)
		grid.addWidget(self.le_a2is,8,2)
		grid.addWidget(self.le_b2is,9,2)

		#bottom buttons
		grid.addWidget(self.btn_help,11,0)
		grid.addWidget(self.btn_decipher,10,2)
		grid.addWidget(self.btn_encrypt,11,2)

		self.setLayout(grid)
		self.btn_decipher.clicked.connect(lambda:self.decipher(int(self.le_ais.text()),int(self.le_bis.text()),int(self.le_a2is.text()), int(self.le_b2is.text())))
		self.btn_encrypt.clicked.connect(lambda:self.encrypt(int(self.le_ais.text()),int(self.le_bis.text()),int(self.le_a2is.text()), int(self.le_b2is.text())))

		self.setGeometry(600,300,650,150)
		self.setWindowTitle('Affine cipher')
		self.show()

	def gcd (self,_a,_b):
		while _b:
			_a, _b = _b, _a%_b
		return _a;

	def findModInverse (self,a,m):
		if self.gcd(a,m) != 1:
			return 0
		u1, u2, u3 = 1, 0, a
		v1, v2, v3 = 0, 1, m
		while v3 != 0:
			q = u3 // v3
			v1, v2, v3, u1, u2, u3 = (u1 - q * v1), (u2 - q * v2), (u3 - q * v3), v1, v2, v3
		return u1 % m


	def checkab(self,a,b):
		if self.gcd(self.textlen,a) != 1:
			#QMessageBox.information(self,"a",str(self.textlen)+" | "+str(a))
			QMessageBox.warning(self,"Ошибка","Секретный ключ не соответвует требованиям. \n В качестве ключа a необходимо выбрать "
			+ "целое взаимно простое с " + str(self.textlen) + " число.")
			return False

		if b > self.textlen or b < 0:
			QMessageBox.warning(self,"Ошибка","Секретный ключ не соответвует требованиям. \n В качестве " 
			+ "ключа b необходимо выбрать ключ от 0 до " + str(self.textlen))
			return False
		return True

	def encrypt(self,a,b,a2,b2):
		#Вычисляет длинну алфавита
		self.textlen = len(self.te_alphabettext.toPlainText())
		self.lb_alphabettext.setText ("Длинна алфавита " + str(self.textlen))
		if self.mode == 0:
			translated = self.affinEncrypt(a,b)
			self.te_ciphertext.setText(translated)
		if self.mode == 1:
			translated = self.reqaffinEncrypt(a,b,a2,b2)
			self.te_ciphertext.setText(translated)
			#QMessageBox.critical(self,"Ошибка","рекуррентный метод не реализован") 

	def decipher(self,a,b,a2,b2):
		#Вычисляет длинну алфавита
		self.textlen = len(self.te_alphabettext.toPlainText())
		self.lb_alphabettext.setText ("Длинна алфавита " + str(self.textlen))
		if self.mode == 0:
			translated = self.affinDecipher(a,b)
			self.te_ciphertext.setText(translated)
		if self.mode == 1:
			translated = self.reqaffinDecipher(a,b,a2,b2)
			self.te_ciphertext.setText(translated)
			#QMessageBox.critical(self,"Ошибка","рекуррентный метод не реализован") 

	def affinEncrypt(self,a,b):

		#Проверка верность a и b
		if (self.checkab(a,b) == False): return 1
		ciphertext = ""
		for symbol in self.te_opentext.toPlainText():
			alphabet = self.te_alphabettext.toPlainText()
			if symbol in alphabet:
				symIndex = alphabet.find(symbol)
				ciphertext += alphabet[(symIndex*a+b)%len(alphabet)]
			else:
				ciphertext += symbol
		return ciphertext

	def affinDecipher(self,a,b):

		#Проверка верность a и b
		if (self.checkab(a,b) == False): return 1
		ciphertext = ""
		#a_inv = gcd(a,self.te_alphabettext.toPlainText())
		a_inv = self.findModInverse(a,self.textlen)
		for symbol in self.te_opentext.toPlainText():
			alphabet = self.te_alphabettext.toPlainText()
			if symbol in alphabet:
				symIndex = alphabet.find(symbol)
				ciphertext += alphabet[(symIndex-b)*a_inv%len(alphabet)]
			else:
				ciphertext += symbol
		return ciphertext

	def reqaffinEncrypt(self,a,b,a2,b2):
		#QMessageBox.critical(self,"Ошибка",str(a)+str(b)+str(a2)+str(b2)) 
		#Проверка верность a и b a2 b2
		if (self.checkab(a,b) == False or self.checkab(a2,b2) == False): return 1
		ciphertext = ""
		i=0
		arr = []
		brr = []
		arr.append(a)
		brr.append(b)
		arr.append(a2)
		brr.append(b2)
		for symbol in self.te_opentext.toPlainText():
			alphabet = self.te_alphabettext.toPlainText()
			if symbol in alphabet:
				symIndex = alphabet.find(symbol)
				print (symIndex)
				#print (symbol)
				#print("a "+str(arr[i])+" b "+str(brr[i]))
				ciphertext += alphabet[(symIndex*arr[i]+brr[i])%len(alphabet)]
				arr.append((arr[i]*arr[i+1])%len(alphabet))
				brr.append(brr[i]+brr[i+1])
				
				i+=1
			else:
				ciphertext += symbol
		return ciphertext

	def reqaffinDecipher(self,a,b,a2,b2):
		#QMessageBox.critical(self,"Ошибка",str(a)+str(b)+str(a2)+str(b2))
		#Проверка верность a и b a2 b2
		if (self.checkab(a,b) == False or self.checkab(a2,b2) == False): return 1
		ciphertext = ""
		a_inv = self.findModInverse(a,self.textlen)
		a2_inv = self.findModInverse(a2,self.textlen)
		i=0
		arr = []
		brr = []
		arr.append(a_inv)
		brr.append(b)
		arr.append(a2_inv)
		brr.append(b2)
		for symbol in self.te_opentext.toPlainText():
			alphabet = self.te_alphabettext.toPlainText()
			if symbol in alphabet:
				symIndex = alphabet.find(symbol)
				#print (symbol)
				#print("a "+str(arr[i])+" b "+str(brr[i]))
				ciphertext += alphabet[(symIndex-brr[i])*arr[i]%len(alphabet)]

				#ciphertext += alphabet[(symIndex*arr[i]+brr[i])%len(alphabet)]
				#ainv = self.findModInverse(arr[i]*arr[i+1],self.textlen)
				arr.append((arr[i]*arr[i+1])%len(alphabet))
				brr.append((brr[i]+brr[i+1])%len(alphabet))
				
				i+=1
			else:
				ciphertext += symbol
		return ciphertext
	def btnstate(self,b):

		if b.text() == "Аффинный шифр":
			if b.isChecked() == True:
				self.mode = 0
				self.le_a2is.setDisabled(True)
				self.le_a2is.setText("0")
				self.le_b2is.setDisabled(True)
				self.le_b2is.setText("0")
				#self.te_alphabettext.setText(str(self.mode))
		if b.text() == "Аффинный рекуррентный шифр":
			if b.isChecked() == True:
				self.mode = 1
				self.le_a2is.setDisabled(False)
				self.le_b2is.setDisabled(False)
				#self.te_alphabettext.setText(str(self.mode))

	def helpshow(self):
		QMessageBox.information(self,"Помощь","В аффинном шифре каждой букве алфавита размера " +
			"m ставится в соответствие число из диапазона 0 .. m-1. Затем при помощи модульной арифметики" +
			" для каждого числа, соответствующего букве исходного алфавита, вычисляется новое число, " +
			"которое заменит старое в шифротексте."
			+ "\n Разработчик программы: Беляков Данила")

#main function
if __name__ == '__main__':

	app = QApplication (sys.argv)

	exe = GuiView()


	sys.exit(app.exec_())
