#/usr/bin/env python3 
# -*- coding: utf-8 -*-

import sys
import math
import collections as col
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

#import utils
from ui_vigenere import ui_window
#from hillkey import TableSelect

class vigwindow (QWidget, ui_window):
	def __init__ (self):
		super(vigwindow, self).__init__()
		self.setupUi(self)
		self.mode = 0
		#self.keygenerator()
		self.checkbox.clicked.connect(lambda:self.checkgen())
		self.btn_encrypt.clicked.connect(lambda:self.encrypt())
		self.btn_decipher.clicked.connect(lambda:self.decrypt())
		self.show()

	def checkgen (self):
		if (self.mode == 0):
			self.mode = 1
			return
		if (self.mode == 1):
			self.mode = 0

	def encrypt (self):
		alphabet = list(self.te_alphabettext.toPlainText())
		ciphertext = ''
		arr = []
		count = 0
		alphabet = self.te_alphabettext.toPlainText()
		opentextlenght = len(self.te_opentext.toPlainText())
		if (self.mode):
		#if (self.checkbox):
		#if (self.checkbox.stateChanged.connect(self.checkgen)):
			#print ("ON " + str(self.mode))
			#key = col.deque(self.keygenerator())
			self.te_ciphertext.setText(self.keygenerator())
			return
		else:
			#print ("OFF " + str(self.mode))
			key = col.deque(self.te_key.text())
		#Длина ключа (гаммы) должна совпадать с длиной открытого текста
		keydict = []
		for i in range (opentextlenght):
			keydict.append(key[0])
			key.rotate(-1)
		i = 0
		for symbol in self.te_opentext.toPlainText():
			if symbol in alphabet:
				symIndex = alphabet.find(symbol)
				keyIndex = alphabet.find(keydict[i])
				#print (keyIndex)
				ciphertext += alphabet[(symIndex+keyIndex)%len(alphabet)]
				i+=1
			else:
				ciphertext += symbol
			#print (ciphertext)
		self.te_ciphertext.setText(str(ciphertext))

	def decrypt (self):
		alphabet = list(self.te_alphabettext.toPlainText())
		ciphertext = ''
		arr = []
		count = 0
		#print ("its gay")
		alphabet = self.te_alphabettext.toPlainText()
		opentextlenght = len(self.te_opentext.toPlainText())
		if (self.mode):
			#print ("ON " + str(self.mode))
			self.te_ciphertext.setText(self.keygenerator_decrypt())
			return
		else:
			#print ("OFF " + str(self.mode))
			key = col.deque(self.te_key.text())
		#Длина ключа (гаммы) должна совпадать с длиной открытого текста
		keydict = []
		for i in range (opentextlenght):
			keydict.append(key[0])
			key.rotate(-1)
		i = 0
		for symbol in self.te_opentext.toPlainText():
			if symbol in alphabet:
				symIndex = alphabet.find(symbol)
				keyIndex = alphabet.find(keydict[i])
				#print (keyIndex)
				ciphertext += alphabet[(symIndex-keyIndex)%len(alphabet)]
				i+=1
			else:
				ciphertext += symbol
			#print (ciphertext)
		self.te_ciphertext.setText(str(ciphertext))
	
	def keygenerator(self):
		keysimbol = self.te_key.text()[0]
		newkey = ""
		cipher = ""
		print (keysimbol)
		k = 0
		alphabet = (self.te_alphabettext.toPlainText())
		opent = (self.te_opentext.toPlainText())
		for symbol in opent:
			if keysimbol in alphabet:
				symIndex = alphabet.find(symbol)
				if (k==0):
					keyIndex = alphabet.find(keysimbol)
					newkey = (symIndex+keyIndex)%len(alphabet)
					cipher += alphabet[newkey]
					#print ("newkey= "+str(symIndex)+"+"+str(keyIndex)+"="+str(newkey))
				if (k>0):
					oldket = newkey
					cipher += alphabet[(symIndex+newkey)%len(alphabet)]
					newkey = (symIndex+newkey)%len(alphabet)
					#print ("newkey= "+str(symIndex)+"+"+str(oldket)+"="+str(newkey))
				k+=1
		print (keysimbol)
		return cipher

	def keygenerator_decrypt(self):
		keysimbol = self.te_key.text()[0]
		newkey = ""
		cipher = ""
		#print (keysimbol)
		k = 0
		alphabet = (self.te_alphabettext.toPlainText())
		opent = (self.te_opentext.toPlainText())
		for symbol in opent:
			if keysimbol in alphabet:
				symIndex = alphabet.find(symbol)
				if (k==0):
					keyIndex = alphabet.find(keysimbol)
					newkey = (symIndex-keyIndex)%len(alphabet)
					cipher += alphabet[newkey]
					c1 = symIndex
					#print ("newkey= "+str(symIndex)+"-"+str(keyIndex)+"="+str(newkey))
				if (k>0):
					#newkey = (c1-symIndex)%len(alphabet)
					cipher += alphabet[(symIndex-c1)%len(alphabet)]
					#print ("newkey= "+str(symIndex)+"-"+str(c1)+"="+str(newkey))
					c1 = symIndex
				k+=1
		print (keysimbol+" : "+cipher)
		return cipher

if __name__ == '__main__':
	app = QApplication(sys.argv)


	window = vigwindow()
	sys.exit(app.exec_())
