#!#/usr/bin/env python3 
# -*- coding: utf-8 -*-

import math

n = 713
e = 13
#ci = 12345649
ciorg = 47

ci = ciorg
for i in range (25):
	ci = (ci**e)%n
	print ("{0} : {1}".format(i+1,ci))
	if (ci == ciorg):
		break
