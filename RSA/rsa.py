#!#/usr/bin/env python3 
# -*- coding: utf-8 -*-

import sys
import math
import random
import time
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from ui_rsa import ui_window

opentext = "CAT"

class rsawindow(QWidget, ui_window):
	"""docstring for rsa"""
	def __init__(self):
		super(rsawindow, self).__init__()
		self.setupUi(self)
		self.show()
		

def gcd(a, b):
	while b != 0:
		a, b = b, a % b
	return a

def modInv(a,p): #Find inverse of a mod p, if it exists
	for i in range(2,p):
		if (i*a)%p==1:
			return i
		if (i > p):
			break
	return 0

def genpair (p, q):
	if (p==q):
		raise ValueError("p и q не удоволетворяют условию")
	n=p*q
	phi=(p-1)*(q-1)
	#Выбрать e, такое что 1<e<phi и e НОД(e,phi) =1
	while (1):
		e = random.randrange(1,phi)
		#e = 11
		d = modInv(e,phi)
		print (gcd(e,phi))
		if ((d!=0) or (gcd(e,phi)==1)):
			break
	return ((e,n), (d,n))

def asciitobinary (opentext):
	bitopentext = ''
	# Используем 8 бит (Because 7-bit ASCII is retarded (c))
	for symbol in opentext:
		bitopentext += "{0:08b}".format(ord(symbol))
	return bitopentext

def encrypt (openkey,opentext):
	key, n = openkey
	blocklen = (len("{0:b}".format(n)))
	#bitopentext = asciitobinary(opentext)
	#Ну теперь можно и шифровать
	listblockcipher = []
	bitlistblockcipher = ''
	for b in opentext:
		cipher = (ord(b)**key)%n
		listblockcipher.append(cipher)
		bitlistblockcipher +=("{0:0{1}b}".format(cipher,blocklen))
	return listblockcipher, bitlistblockcipher

def decrypt (cipherkey,ciphertext):
	key, n = cipherkey
	blocklen = (len("{0:b}".format(n)))
	#Обрабатываем принятую последовательность битов
	listblock = []
	while (blocklen < len(ciphertext)):
		listblock.insert(0,ciphertext[-blocklen:])
		ciphertext = ciphertext[:-blocklen]
	#Обрабатываем остатки данных, если есть
	if (len(ciphertext) != 0):
		listblock.insert(0,ciphertext.zfill(blocklen))
	#print ("Новая битовая последовательность: {0}".format(listblock))
	opentext = ''
	for b in listblock:
		opensymbol = (int(b,2)**key)%n
		opentext += chr(opensymbol)
	return opentext

if __name__ == "__main__":
	p = int(sys.argv[1])
	q = int(sys.argv[2])

	#Генерируем новую ключевую пару
	openkey, cipherkey = genpair(p,q)
	print ("Открытый ключ: {0}\nЗакрытый ключ: {1}".format(openkey,cipherkey))

	enc, bitenc = encrypt(openkey,opentext)
	print ("Защифрованное сообщение: {0}: {1}".format(enc,bitenc))

	dcpt = decrypt(cipherkey,bitenc)
	print ("Расшифрованное сообщение: {0}".format(dcpt))
