# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtGui,QtWidgets

class ui_window (object):
	def setupUi (self, uiwindow):
		uiwindow.setWindowTitle("RSA")
		uiwindow.setGeometry(600,300,600,300)


		self.te_opentext = QtWidgets.QTextEdit()
		self.te_ciphertext = QtWidgets.QTextEdit()
		self.te_alphabettext  = QtWidgets.QTextEdit()
		self.te_key = QtWidgets.QLineEdit()
		self.te_key2 = QtWidgets.QLineEdit()


		#self.btn_usetable = QtWidgets.QPushButton ('Редактор ключа', self)
		self.te_alphabettext.setText("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
		#self.rb_affine  = QtWidgets.QRadioButton("Классический")
		#self.rb_affine.setChecked(True)
		#self.rb_affinereq  = QtWidgets.QRadioButton("Рекуррентный")
		self.btn_encrypt = QtWidgets.QPushButton ('Зашифровать', self)
		self.btn_decipher = QtWidgets.QPushButton ('Расшифровать', self)
		self.te_key.setText("31")
		self.te_key2.setText("29")
		#self.te_key2.setDisabled(True)

		self.checkbox = QtWidgets.QCheckBox ('Подобрать экпоненту', self);

		layout = QtWidgets.QGridLayout()
		layout.addWidget(QtWidgets.QLabel("Исходный текст"),1,0)
		layout.addWidget(QtWidgets.QLabel("Битовая последовательность"),1,2)
		layout.addWidget(self.te_opentext,2,0)
		layout.addWidget(self.te_ciphertext,2,2,1,2)
		#layout.addWidget(QtWidgets.QLabel("Алфавит"),3,0)
		#layout.addWidget(self.te_alphabettext,4,0,5,1)

		layout.addWidget(QtWidgets.QLabel("P"),4,2)
		layout.addWidget(QtWidgets.QLabel("Q"),5,2)
		layout.addWidget(QtWidgets.QLabel("e"),6,2)
		layout.addWidget(self.te_key,4,3)
		layout.addWidget(self.te_key2,5,3)
		layout.addWidget(self.checkbox,6,3)
		layout.addWidget(self.btn_encrypt,9,0)
		layout.addWidget(self.btn_decipher,9,2,1,2)
		uiwindow.setLayout(layout)