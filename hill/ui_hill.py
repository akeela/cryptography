#/usr/bin/env python3 
# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtGui,QtWidgets
#from PyQt5.QtWidgets import QGridLayout,QLabel

class ui_hillwindow (object):
	def setupUi (self, hillwindow):
		hillwindow.setWindowTitle("Hill's cipher")
		hillwindow.setGeometry(600,300,600,300)

		self.te_opentext = QtWidgets.QTextEdit()
		self.te_ciphertext = QtWidgets.QTextEdit()
		self.te_alphabettext  = QtWidgets.QTextEdit()
		self.te_key = QtWidgets.QLineEdit()
		self.te_key2 = QtWidgets.QLineEdit()


		#self.btn_usetable = QtWidgets.QPushButton ('Редактор ключа', self)
		self.te_alphabettext.setText("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
		self.rb_affine  = QtWidgets.QRadioButton("Классический")
		self.rb_affine.setChecked(True)
		self.rb_affinereq  = QtWidgets.QRadioButton("Рекуррентный")
		self.btn_encrypt = QtWidgets.QPushButton ('Зашифровать', self)
		self.btn_decipher = QtWidgets.QPushButton ('Расшифровать', self)
		self.te_key.setText("2 0 3; 23 5 11; 7 6 25")
		self.te_key2.setText("2 0 3; 23 5 11; 7 6 25")
		self.te_key2.setDisabled(True)

		layout = QtWidgets.QGridLayout()
		layout.addWidget(QtWidgets.QLabel("Исходный текст"),1,0)
		layout.addWidget(QtWidgets.QLabel("Измененный текст"),1,2)
		layout.addWidget(self.te_opentext,2,0)
		layout.addWidget(self.te_ciphertext,2,2)
		layout.addWidget(QtWidgets.QLabel("Алфавит"),3,0)
		layout.addWidget(self.te_alphabettext,4,0,5,1)
		#layout.addWidget(QtWidgets.QLabel("Ключ:"),4,2)
		layout.addWidget(QtWidgets.QLabel("Шифр:"),3,2)
		layout.addWidget(self.rb_affine,4,2)
		layout.addWidget(self.rb_affinereq,5,2)
		layout.addWidget(QtWidgets.QLabel("Ключ:"),6,2)
		layout.addWidget(self.te_key,7,2)
		layout.addWidget(self.te_key2,8,2)
		layout.addWidget(self.btn_encrypt,9,0)
		layout.addWidget(self.btn_decipher,9,2)
		hillwindow.setLayout(layout)
		#hillwindow.setObjectName("hillwindow")
		#hillwindow.resize(600,400)
		#self.centralWidget = QtWidgets.QWidget(hillwindow)
		#self.centralWidget.setObjectName("centralWidget")
		#self.le_label = QtWidgets.QLabel("Исходный текст")
		#self.centralWidget.addWidget(QtWidgets.QLabel("Исходный текст"),1,0)
		#self.leftLayout = QtWidgets.QLabel("Исходный текст")
		#QLabel("Исходный текст")
		#hillwindow.setLayout(self.grid)
		#hillwindow.setCentralWidget(self.centralWidget)
		#QtCore.QMetaObject.connectSlotsByName(hillwindow)
