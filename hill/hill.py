#/usr/bin/env python3 
# -*- coding: utf-8 -*-

import sys
import numpy as np
import math
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

import utils
from ui_hill import ui_hillwindow
#from hillkey import TableSelect

class hillwindow (QWidget, ui_hillwindow):
	def __init__ (self):
		super(hillwindow, self).__init__()
		self.setupUi(self)
		self.mode = 0
		defaultmatrix2 = [[2,3],
						[5,7]]
		defaultmatrix = [[2,0,3],
						[23,5,11],
						[7,6,25]]

		self.rb_affine.toggled.connect(lambda:self.btnstate(self.rb_affine))
		self.rb_affinereq.toggled.connect(lambda:self.btnstate(self.rb_affinereq))
		self.btn_encrypt.clicked.connect(lambda:self.encrypt(self.setkey(),self.setkey2(),self.te_opentext.toPlainText()))
		self.btn_decipher.clicked.connect(lambda:self.decrypt(self.setkey(),self.setkey2(),self.te_opentext.toPlainText()))
		self.show()

	def setkey (self):
		return np.array(np.mat(self.te_key.text()))

	def setkey2 (self):
		return np.array(np.mat(self.te_key2.text()))

	def btnstate(self,b):

		if b.text() == "Классический":
			if b.isChecked() == True:
				self.mode = 0
				self.te_key2.setDisabled(True)
		if b.text() == "Рекуррентный":
			if b.isChecked() == True:
				self.mode = 1
				self.te_key2.setDisabled(False)

	def encrypt (self, matrix, matrix2, words):
		if(self.checkparam(matrix, matrix2, words)==1): return 1
		if self.mode == 0:
			translated = self.hillencrypt(matrix,words)
		if self.mode == 1:
			translated = self.reqhillencrypt(matrix, matrix2, words)

	def decrypt (self, matrix, matrix2, words):
		if(self.checkparam(matrix, matrix2, words)==1): return 1
		if self.mode == 0:
			translated = self.hilldecrypt(matrix,words)
		if self.mode == 1:
			translated = self.reqhilldecrypt(matrix, matrix2, words)

	def hillencrypt (self, matrix, words):
		cipherchar = ''
		alphabet = self.te_alphabettext.toPlainText()
		ciphertext = ""
		#Считает длинну матрицы (2,3)
		length = len(matrix)
		matrix = np.array(matrix) #пусть будет
		#Сличате длинну алфавита, по дефолту это 26
		alphalen = len(alphabet)
		arr = []
		count = 0
		for symbol in self.te_opentext.toPlainText():
			textlen = len(self.te_opentext.toPlainText())
			if (textlen % length):
				self.te_opentext.insertPlainText(self.te_alphabettext.toPlainText()[-1:])
		for symbol in self.te_opentext.toPlainText():
			if (symbol in alphabet):
				symIndex = alphabet.find(symbol)
				arr.append(symIndex)
				count +=1
				#Считает блоки, после начинает цикл построения заного
				if (count == length):
					cipherchar = (np.dot(matrix,arr)%alphalen)
					for i in cipherchar:
						ciphertext += alphabet[i]
					count = 0
					arr = []
		#Вносим в шифротекст то что осталось (незнаю, верно ли это)
		if count > 0:
			print (count)

			for i in arr:
				ciphertext += alphabet[i]

		self.te_ciphertext.setText(ciphertext)


	def hilldecrypt (self, matrix, words):
		cipherchar = ''
		alphabet = self.te_alphabettext.toPlainText()
		#Сличате длинну алфавита, по дефолту это 26
		alphalen = len(alphabet)
		ciphertext = ""
		#Считает длинну матрицы (2,3)
		length = len(matrix)
		arr = []
		count = 0
		#АААААА 7 ЧАСОВ ПОТРАТИЛ НА ЭТУ ДЕШИФРАЦИЮ
		matrix = utils.modMatInv(matrix,alphalen)
		for symbol in self.te_opentext.toPlainText():
			textlen = len(self.te_opentext.toPlainText())
			if (textlen % length):
				self.te_opentext.insertPlainText(self.te_alphabettext.toPlainText()[-1:])
		for symbol in self.te_opentext.toPlainText():
			if (symbol in alphabet):
				symIndex = alphabet.find(symbol)
				arr.append(symIndex)
				count +=1
				#Считает блоки, после начинает цикл построения заного
				if (count == length):
					cipherchar = np.around(np.dot(matrix,arr))% alphalen
					#НЕНАВИЖУ
					for i in cipherchar:
						ciphertext += (alphabet[int(i)])
					count = 0
					arr = []
		#Вносим в шифротекст то что осталось (незнаю, верно ли это)
		for i in arr:
			ciphertext += alphabet[i]
		self.te_ciphertext.setText(ciphertext)

	def reqhillencrypt (self, matrix, matrix2, words):
		cipherchar = ''
		alphabet = self.te_alphabettext.toPlainText()
		ciphertext = ""
		#Считает длинну матрицы (2,3)
		length = len(matrix)
		matrix = np.array(matrix) #пусть будет
		matrix2 = np.array(matrix2)
		twokeyflag = 0
		#Сличате длинну алфавита, по дефолту это 26
		alphalen = len(alphabet)
		arr = []
		count = 0
		for symbol in self.te_opentext.toPlainText():
			textlen = len(self.te_opentext.toPlainText())
			if (textlen % length):
				self.te_opentext.insertPlainText(self.te_alphabettext.toPlainText()[-1:])
		for symbol in self.te_opentext.toPlainText():
			if (symbol in alphabet):
				symIndex = alphabet.find(symbol)
				arr.append(symIndex)
				count +=1
				#Считает блоки, после начинает цикл построения заного
				if (count == length):
					if (twokeyflag == 0):
						matrix3 = matrix
					if (twokeyflag == 1):
						matrix3 = matrix2
					if (twokeyflag > 1):
						matrix3 = (np.dot(matrix,matrix2))%alphalen
						print ("matrix3")
						matrix = matrix2
						matrix2 = matrix3
					twokeyflag+=1
					print (matrix3)
					cipherchar = (np.dot(matrix3,arr)%alphalen)
					#print (cipherchar)
					for i in cipherchar:
						ciphertext += alphabet[i]
					count = 0
					arr = []
		#Вносим в шифротекст то что осталось (незнаю, верно ли это)
		for i in arr:
			ciphertext += alphabet[i]

		self.te_ciphertext.setText(ciphertext)

	def reqhilldecrypt (self, matrix, matrix2, words):
		cipherchar = ''
		alphabet = self.te_alphabettext.toPlainText()
		#Сличате длинну алфавита, по дефолту это 26
		alphalen = len(alphabet)
		ciphertext = ""
		#Считает длинну матрицы (2,3)
		length = len(matrix)
		twokeyflag = 0
		arr = []
		count = 0
		matrix = utils.modMatInv(matrix,alphalen)
		matrix2 = utils.modMatInv(matrix2,alphalen)
		for symbol in self.te_opentext.toPlainText():
			textlen = len(self.te_opentext.toPlainText())
			if (textlen % length):
				self.te_opentext.insertPlainText(self.te_alphabettext.toPlainText()[-1:])
		for symbol in self.te_opentext.toPlainText():
			if (symbol in alphabet):
				symIndex = alphabet.find(symbol)
				arr.append(symIndex)
				count +=1
				#Считает блоки, после начинает цикл построения заного
				if (count == length):
					if (twokeyflag == 0):
						matrix3 = matrix
					if (twokeyflag == 1):
						matrix3 = matrix2
					if (twokeyflag > 1):
						try:
							#matrix3 = ((matrix2*matrix)+alphalen)%alphalen
							#matrix3 = utils.modMatInv((np.dot(matrix2,matrix)),alphalen)
							matrix3 = np.dot(matrix2,matrix)
						except:
							print ('Немогу посчитать:c')
							#matrix3 = (np.linalg.inv(matrix2*matrix)+alphalen)% alphalen
						#print (matrix3)
						matrix = matrix2
						matrix2 = matrix3
						print (matrix3)
					twokeyflag+=1
					#cipherchar = (np.dot(matrix3,arr)%alphalen)
					cipherchar = np.around(np.dot(matrix3,arr))% alphalen
					#print (cipherchar)
					for i in cipherchar:
						ciphertext += (alphabet[int(i)])
					count = 0
					arr = []
		#Вносим в шифротекст то что осталось (незнаю, верно ли это)
		for i in arr:
			ciphertext += alphabet[i]
		self.te_ciphertext.setText(ciphertext)

	def checkparam (self, matrix, matrix2, words):
		try:
			inverse = np.linalg.inv(matrix)
			inverse = np.linalg.inv(matrix2)
		except:
			QMessageBox.warning(self,"Ошибка","Ключ не имеет обратной матрицы. Впишите другой ключ")
			return 1
			#exit()
	
	def keyselect(self):
		pass

if __name__ == '__main__':
	app = QApplication(sys.argv)


	window = hillwindow()
	sys.exit(app.exec_())
